{{$tabs}}
<h2>{{$title}}</h2>

<a class="Button RoundCorners" style="display:block;" href="{{$new_event.0}}">{{$new_event.1}}</a>

<!--<noscript> -->
<div class="Debug">
  This is just temporary, because the real calendar isn't working yet!
</div>
<div>
  <div class="EventList Border RoundCorners">
  {{foreach $events as $event}}
    <div class="Event2 Border RoundCorners Padding Margin SmallText">
    {{$event.html}}
    {{if $event.item.author_name}}<a href="{{$event.item.author_link}}" ><img src="{{$event.item.author_avatar}}" height="32" width="32" />{{$event.item.author_name}}</a>{{/if}}
    
    {{if $event.item.plink}}<a href="{{$event.plink.0}}" title="{{$event.plink.1}}" target="_blank" class="plink-event-link icon s22 remote-link"></a>{{/if}}
    {{if $event.edit}}<a href="{{$event.edit.0}}" title="{{$event.edit.1}}" class="edit-event-link icon s22 pencil"></a>{{/if}}
    </div>
  {{/foreach}}
  </div>
</div>
<!-- </noscript> -->
<div id="CalendarWrapper" class="Margin JS">
  <div clasS="Border">
    <span class="Button RoundCorners Cell" onclick="javascript:void(FriendicaUI.Calendar.PreviousInterval());">&lt;</span>
    <span class="Button RoundCorners Cell" onclick="javascript:void(FriendicaUI.Calendar.NextInterval());">&gt</span>
    <span class="Button RoundCorners Cell" onclick="javascript:void(FriendicaUI.Calendar.JumpToCurrentDate());">Now</span>
    <span id="Calendar_Month" class="Headline Center Cell" style="width:70%;">TITLE</span>
    <span class="Button RoundCorners Cell" onclick="javascript:void(FriendicaUI.Calendar.MonthView());">month</span>
    <span class="Button RoundCorners Cell" onclick="javascript:void(FriendicaUI.Calendar.WeekView());">week</span>
    <span class="Button RoundCorners Cell" onclick="javascript:void(FriendicaUI.Calendar.DayView());">day</span>
  </div>
  <table id="Calendar" class="Border">
    <tr id="Calendar_Row_Template" class="Template">
      <td id="Calendar_Cell_Template" class="Border Padding" >
        <h4 name="Title"></h4>
        <div name="Description">Description</div>
      </td>
    </tr>
  </table>
  <div class="Debug">
    Old calendar, just for debug and re-engineering:
  </div>
  <div id="events-calendar">
      <!-- just for development -->
  </div>
</div>