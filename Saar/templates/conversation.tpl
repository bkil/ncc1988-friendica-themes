
{{$live_update}}

{{foreach $threads as $thread}}
<div id="Post-{{$t.id}}" class="Post Padding Border RoundCorners Layer2">
  {{foreach $thread.items as $item}}
    {{include file="{{$item.template}}"}}
  {{/foreach}}
</div>
{{/foreach}}
