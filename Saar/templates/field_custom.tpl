<div class="FieldRow">
  <div class="FieldCell">{{$field.1}}</div>
  <div class="FieldCell">
    {{$field.2}}
    <div class="FieldHint">{{$field.3}}</div>
  </div>
</div>