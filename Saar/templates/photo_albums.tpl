{{$widget = ["title" => $title, "parts" => [
  ["type" => "button", "content" => ["action" => "`$baseurl`/photos/`$nick`", "title" => $recent]]
  ]]
}}
{{if $albums}}
  {{foreach $albums as $a}}
    {{if $a.text}}
      {{$widget.parts[] = ["type" => "link", "content" => ["action" => "`$baseurl`/photos/`$nick`/album/`$a.bin2hex`", "title" => "`$a.text` (`$a.total`)"]
        ]
      }}
    {{/if}}
  {{/foreach}}
{{/if}}
{{if $can_post}}
  {{$widget.parts[] = ["type" => "button", "content" => ["action" => $upload.1, "title" => $upload.0]]}}
{{/if}}
{{include file="FTI_Widget.tpl" widget=$widget}}
