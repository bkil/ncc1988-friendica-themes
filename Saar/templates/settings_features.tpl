{{$blocks = []}}

{{$blocks[] = ["type" => "title", "title" => $title, "layer" => 1]}}

{{foreach $features as $feature}}
  {{$formBlock = ["type" => "form", "title" => $feature.0, "layer" => 2]}}
  {{$form = ["action" => "settings/features", "method" => "post", "autocomplete" => false, "elements" => [], "submitValue" => $submit]}}
  
  {{foreach $feature.1 as $element}}
    {{$form.elements[] = ["type" => "checkbox", "name" => $element.0, "value" => $element.2, "title" => $element.1, "description" => $element.3]}}
  {{/foreach}}
  
  {{$formBlock.content = $form}}
  {{$blocks[] = $formBlock}}
{{/foreach}}

{{foreach $blocks as $block}}
  {{include file="FTI_PageBlock.tpl" pageBlock=$block}}
{{/foreach}}
