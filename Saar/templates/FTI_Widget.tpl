<div
  {{if $widget.idattr}}
    id="{{$widget.idattr}}"
  {{/if}}
  {{if $widget.isSideWidget}}
    class="SideWidget BottomLine">
  {{else}}
    {{if $widget.layer == 1}}
      class="Widget Border RoundCorners Padding Layer1">
    {{elseif $widget.layer == 2}}
      class="Widget Border RoundCorners Padding Layer2">
    {{else}}
      class="Widget Border RoundCorners Padding Layer3">
    {{/if}}
  {{/if}}
  {{if $widget.title}}
    <h3>{{$widget.title}}</h3>
  {{/if}}
  {{if $widget.description}}
    <p class="Center">{{$widget.description}}</p>
  {{/if}}
  {{foreach $widget.parts as $part}}
    {{if $part.type == "form"}}
      {{include file="FTI_Form.tpl" form=$part.content}}
    {{elseif $part.type == "button"}}
      <a class="Button Block RoundCorners {{if $part.content.selected}}Selected{{/if}}"
        {{if $part.content.action}}
        href="{{$part.content.action|escape:'html'}}"
        {{/if}}
        >
        {{if $part.content.iconURL}}
        <img class="Button RoundCorners SmallIcon SmallText" src="{{$part.content.iconURL|escape:'html'}}"
          alt="{{$part.content.title|escape:'html'}}"
          {{if $part.content.jsEvent}}
            {{$part.content.jsEvent.event}}="{{$part.content.jsEvent.action}}"
          {{/if}}
          ></img>
        {{else}}
          {{$part.content.title|escape:'html'}}
        {{/if}}
      </a>
    {{elseif $part.type == "buttonGroup"}}
      {{foreach $part.content.buttons as $button}}
        <a class="Button RoundCorners {{if $button.selected}}Selected{{/if}} {{if not $part.content.alignH}}Block{{/if}}"
        {{if $button.action}}
        href="{{$button.action|escape:'html'}}"
        {{/if}}
        >
        {{if $button.iconURL}}
        <img class="Button RoundCorners SmallIcon SmallText" src="{{$button.iconURL|escape:'html'}}"
          alt="{{$button.title|escape:'html'}}"
          {{if $button.jsEvent}}
            {{$button.jsEvent.event}}="{{$button.jsEvent.action}}"
          {{/if}}
          ></img>
        {{else}}
          {{$button.title|escape:'html'}}
        {{/if}}
      </a>
      {{/foreach}}
    {{elseif $part.type == "image"}}
      {{if $part.content.url}}
        <a href="{{$part.content.url}}">
          <img class="BigIcon RoundCorners" src="{{$part.content.source}}" alt="{{$part.content.description}}" />
        </a>
      {{else}}
        <img class="BigIcon RoundCorners" src="{{$part.content.source}}" alt="{{$part.content.description}}" />
      {{/if}}
    {{elseif $part.type == "text"}}
      {{if $part.content.size == "big" }}
        <p class="BigText">
      {{elseif $part.content.size == "small"}}
        <p class="SmallText">
      {{else}}
        <p>
      {{/if}}
        {{$part.content.text}}
      </p>
    {{elseif $part.type == "link"}}
      <a href="{{$part.content.action|escape:'html'}}">{{$part.content.title|escape:'html'}}</a>
    {{elseif $part.type == "html"}}
      <div>{{$part.content}}</div>
    {{/if}}
  {{/foreach}}
</div>
