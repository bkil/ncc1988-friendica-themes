{{if $threaded}}
<div class="ActionCommentForm_Wrapper Threaded">
{{else}}
<div class="ActionCommentForm_Wrapper">
{{/if}}
  <form name="NewCommentForm_{{$id}}" class="ActionCommentForm" action="item" method="post"
    onsubmit="FriendicaUI.Post.SendNewComment(this, {{$id}});return false;">
    <input type="hidden" name="type" value="{{$type}}" />
    <input type="hidden" name="profile_uid" value="{{$profile_uid}}" />
    <input type="hidden" name="parent" value="{{$parent}}" />
    <input type="hidden" name="jsreload" value="{{$jsreload}}" />
    <input type="hidden" name="preview" id="comment-preview-inp-{{$id}}" value="0" />
    <input type="hidden" name="post_id_random" value="{{$rand_num}}" />
    
    <div class="ActionCommentForm_Photo">
    </div>
    <textarea class="Textarea Border RoundCorners" id="Post_{{$id}}_NewCommentTextarea" name="body" 
      onkeyup="javascript:void(FriendicaUI.CountChars(this, 'CharCount_Comment_{{$id}}'));"
      onfocus="javascript:void(FriendicaUI.ShowField('CharCount_Comment_{{$id}}_Wrapper', 'visible'));"
      onfocusout="javascript:void(FriendicaUI.HideField('CharCount_Comment_{{$id}}_Wrapper'));"
      placeholder="{{$comment}}"></textarea>
    {{if $qcomment}}
    <select class="ActionCommentForm_QCommentSelect" id="ActionComment_QComment-{{$id}}" name="qcomment-{{$id}}" onchange="qCommentInsert(this,{{$id}});">
      <option value=""></option>
      {{foreach $qcomment as $qc}}
      <option value="{{$qc|escape:'html'}}">{{$qc}}</option>
      {{/foreach}}
    </select>
    {{/if}}
    <input class="Button RoundCorners" id="SubmitComment_{{$id}}" type="submit" name="submit" value="{{$submit|escape:'html'}}" />
    {{if $preview}}
    <span class="FlatButton RoundCorners" onclick="FriendicaUI.PreviewText({{$id}});">{{$preview}}</span>
    {{/if}}
    <div id="CharCount_Comment_{{$id}}_Wrapper" class="Right" style="visibility:hidden;">Characters written: <span id="CharCount_Comment_{{$id}}">0</span></div>
    <div class="ActionCommentForm_Preview" id="comment-edit-preview-{{$id}}">
    </div>
  </form>
</div>