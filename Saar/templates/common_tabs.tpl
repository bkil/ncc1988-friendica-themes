<noscript style="display:block;" class="Margin">WARNING: Some functionality is unavailable if JavaScript is disabled!</noscript>
{{$buttons = []}}

{{foreach $tabs as $tab}}
  {{$button = ["action" => $tab.url, "title" => $tab.label]}}
  {{if $tab.sel}}
    {{$button.selected = true}}
  {{/if}}
  {{$buttons[] = $button}}
{{/foreach}}
{{$buttons.alignH = true}}
{{$widget = ["layer" => 1, "isSideWidget" => false, "parts" => [["type" => "buttonGroup", "content" => ["buttons" => $buttons, "alignH" => true]]]]}}
{{include file="FTI_Widget.tpl" widget=$widget}}
