{{$form = ["action" => "removeme", "autocomplete" => false, "method" => "post", "submitValue" => $submit]}}
{{$form.elements = [
  ["type" => "hidden", "name" => "verify", "value" => $hash],
  ["type" => "password", "name" => "qxz_password", "title" => $passwd]
]}}
{{$formWidget = ["type" => "form", "content" => $form]}}
{{$widget = ["title" => $title, "description" => $desc, "layer" => 1, "parts" => [$formWidget]]}}
{{include file="FTI_Widget.tpl" widget=$widget}}
<!--
<form acion="{{$basedir}}/removeme" autocomplete="off" method="post">
  <input type="hidden" name="verify" value="{{$hash}}" />
  <label for="RemoveAccountPassword">{{$passwd}}</label>
  <input type="password" class="InputField RoundCorners" id="RemoveAccountPassword" name="qxz_password" />
  <div class="Center">
    <input class="Button RoundCorners" type="submit" name="submit"  value="{{$submit|escape:'html'}}" />
  </div>
</form>
-->