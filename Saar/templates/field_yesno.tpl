<div class="FieldRow">
  <div class="FieldCell">{{$field.1}}</div>
  <div class="FieldCell">
    <input class="Field RoundCorners" type="checkbox" 
      name="{{$field.0}}"
      value="{{$field.2|escape:'html'}}"
    />
    <label for="{{$field.0}}" class="Border RoundCorners"></label>
    <div class="FieldHint">{{$field.3}}</div>
  </div>
</div>