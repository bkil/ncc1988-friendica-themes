<div class="FieldRow">
  <div class="FieldCell">{{$field.1}}</div>
  <div class="FieldCell">
    <input class="Field Border RoundCorners"
      {{if $field.6 eq 'email'}}
      type="email"
      {{elseif $field.6 eq 'url'}}
      type="url"
      {{/if}}
      name="{{$field.0}}"
      value="{{$field.2|escape:'html'}}"
      {{if $field.4 eq 'required'}} required="required"{{/if}}
      {{if $field.5 eq 'autofocus'}} autofocus="autofocus"{{/if}}
    />
    <div class="FieldHint">{{$field.3}}</div>
  </div>
</div>