<div id="Post-{{$post.id}}" class="Post Padding Border RoundCorners Layer{{$post.layer}}">
  <a name="{{$post.id}}"></a>
  <div class="PostContent" id="Post-{{$post.id}}">
    <div class="PostHeader BottomLine">
      <span class="PostOwnerInfo MenuActivator">
        <a class="Link" href="{{$post.owner.profileUrl}}" target="redir" title="Show profile of {{$post.owner.name}}">
          <img class="SmallIcon RoundCorners" src="{{$post.owner.photoUrl}}" alt="{{$post.owner.name}}" />
          <span>{{$post.owner.name}}</span>
        </a>
        <ul id="PostOwnerMenu-{{$post.id}}" class="Border Menu">
          {{$post.actionListItems}}
        </ul>
      </span>
      <span class="PostDetails SmallText">
        {{if $post.url}}
        <a class="PostTimePassed" title="{{$post.localCreationDate}}" href="{{$post.url}}">{{$post.localCreationDate}}</a>
        {{else}}
        <span class="PostTimePassed" title="{{$post.localCreationDate}}" >{{$post.localCreationDate}}</span>
        {{/if}}
      </span>
    </div>
    <div class="PostContent">
      <h3 class="Headline Underline">{{$post.title}}</h3>
      <div class="PostBody">
        {{$post.body}}
      </div>
      {{if $post.categories}}
        <div class="PostTags">
          {{foreach $post.categories as $c}}
            {{$c.name}}
            {{if $c.actionUrls.remove}}
              <a href="{{$c.actionUrls.remove}}" title="remove">[remove]</a>
            {{/if}}
            &nbsp;
          {{/foreach}}
        </div>
      {{/if}}
      <!-- TODO: Post categories and Post folders -->
      <div class="PostInfos">
        <span class="PostInfoField SmallText">{{$post.info.likes}}</span>
        <span class="PostInfoFiled SmallText">{{$post.info.dislikes}}</span>
      </div>
      <div class="PostActions SmallText">
        <img class="Button RoundCorners PostAction" title="{{$post.action['like'].title|escape:'html'}}"
          onclick="javascript:void(FriendicaUI.Post.Mark({{$post.id}}, 'like'));"
          src="./view/theme/Saar/icons/Like.svg"></img>
        {{if $post.action['dislike']}}
        <img class="Button RoundCorners PostAction" title="{{$post.action.['dislike'].title|escape:'html'}}"
          onclick="javascript:void(FriendicaUI.Post.Mark({{$post.id}},'dislike'));"
          src="./view/theme/Saar/icons/Dislike.svg"></img>
        {{/if}}
        {{if $post.action['share']}}
        <img class="Button RoundCorners PostAction" title="{{$post.action['share'].title|escape:'html'}}"
          onclick="javascript:void(FriendicaUI.Post.Reshare({{$post.id}}));"
          src="./view/theme/Saar/icons/Reshare.svg"></img>
        {{/if}}
      </div>
    </div>
    {{foreach $post.children as $child}}
      <div class="Debug">
        {{$child.template}}
      </div>
      {{include file="FTI_Post.tpl" post=$child layer=$post.layer+1}}
    {{/foreach}}
    
    {{if $item.flatten}}
      {{if $item.comment}}
      <div class="ActionComment Border Layer3 RoundCorners">
        {{$item.comment}}
      </div>
      {{/if}}
    {{/if}}
  </div>
</div>

