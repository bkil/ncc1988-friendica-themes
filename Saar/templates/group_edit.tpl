<h2>{{$title}}</h2>

<div class="Border RoundCorners">
  <form action="group/{{$gid}}" method="post">
    <input type="hidden" name="form_security_token" value="{{$form_security_token}}" />
    {{include file="field_input.tpl" field=$gname}}
    {{if $drop}}
      {{$drop}}
    {{/if}}
    
    {{if $groupeditor}}
    <div class="Border RoundCorners">
      <div class="Border RoundCorners">
        <h3>{{$groupeditor.label_members}}</h3>
        {{foreach $groupeditor.members as $m}}
        {{$m}}
        {{/foreach}}
      </div>
      <div class="Border RoundCorners">
        <h3>{{$groupeditor.label_contacts}}</h3>
        {{foreach $groupeditor.contacts as $c}}
        {{$c}}
        {{/foreach}}
      </div>
      {{if $desc}}
      <p class="Center">{{$desc}}</p>
      {{/if}}
    </div>
    {{/if}}
    
    <div class="Center">
      <input class="Button RoundCorners" name="submit" value="{{$submit|escape:'html'}}" />
    </div>
  </form>
</div>
