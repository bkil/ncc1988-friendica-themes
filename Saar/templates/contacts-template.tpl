<h1>{{$header}}{{if $total}} ({{$total}}){{/if}}</h1>
{{if $finding}}
<h4>{{$finding}}</h4>
{{/if}}

<form class="Border RoundCorners" action="{{$cmd}}" method="get">
  <span>{{$desc}}</span>
  <input type="text" name="search" class="Field Border RoundCorners" value="{{$search|escape:'html'}}" />
  <input type="submit" name="submit" class="Button Border RoundCorners" value="{{$submit|escape:'html'}}" />
</form>

{{$tabs}}

<form class="Border RoundCorners Layer1" action="/contacts/batch/" method="post">
  <div class="Row">
  {{foreach $contacts as $contact}}
    {{include file="contact_template.tpl"}}
  {{/foreach}}
  </div>
  {{foreach $batch_actions as $name=>$action}}
  <input type="submit" name="{{$name}}" class="Button Border RoundCorners" value="{{$action|escape:'html'}}" />
  {{/foreach}}
  </div>
</form>