{{if $count}}
<div class="Border RoundCorners Padding">
  <div>
    <span class="Title">{{$event_title}}</span>
    <a class="Button RoundCorners" href="events/">{{$event_reminders}}</a>
  </div>
  <div class="Border RoundCorners Margin EventList">
    {{foreach $events as $event}}
    <div class="Border RoundCorners Event SmallText Margin Padding">
      <div><a href="events/?id={{$event.id}}">{{$event.title}}</a></div>
      <div>{{$event.date}}</div>
    </div>
    {{/foreach}}
  </div>
</div>
{{/if}}