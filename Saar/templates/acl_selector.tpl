<div class="Popup Background" id="Popup_NewPostACL">
  <span class="Button RoundCorners" onclick="javascript:void(FriendicaUI.ACL.SelectAll());">{{$showall}}</span>
  <input onchange="javascript:void(FriendicaUI.ACL.Search(this, 'ACLResult'));" />
  
  <div id="ACLResults" class="Layer1">
  
    <div id="NewPost_ACLItem_Template" class="Layer2 ACLItem Smalltext Template" data-cid="">
      <img src="" class="ACLItem_Image" />
      <div class="ACLItem_Text">
        <div class="ACLItem_Name">Test</div>
        <div class="ACLItem_Visibility">
          <input type="checkbox" class="ACLItem_Visibility_Checkbox" />
          <span>{{$show}}</span>
        </div>
      </div>
    </div>
    
  </div>
  <hr/>
  <div id="ACL_EMail">
    <span>{{$emailcc}}</span>
    <input type="text" name="emailcc" title="{{$emtitle|escape:'html'}}" />
  </div>
  <span class="Button RoundCorners MainButton" onclick="javascript:void(FriendicaUI.ACL.Apply());">Apply</span>
  <span class="Button RoundCorners" onclick="javascript:void(FriendicaUI.Popup.Close());">Close</span>
</div>