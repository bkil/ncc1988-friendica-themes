{{$widget = []}}
{{if $title}}
  {{$widget.title = $title}}
{{/if}}
{{if $desc}}
  {{$widget.description = $desc}}
{{/if}}
{{if $items}}
  {{$widget.parts = []}}
  {{foreach $items as $item}}
    {{$widget.parts[] = ["type" => "button", "content" => ["action" => $item.url, "selected" => $item.selected, "title" => $item.label]]}}
  {{/foreach}}
{{/if}}
{{include file="FTI_Widget.tpl" widget = $widget}}
