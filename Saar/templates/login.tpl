{{$form = ["action" => $dest_url, "method" => "post"]}}
{{$form.elements = [
  ["type" => "hidden", "name" => "auth-params", "value" => "login"],
  ["type" => "text", "name" => $lname.0, "value" => $lname.2, "title" => $lname.1, "description" => $lname.3],
  ["type" => "password", "name" => $lpassword.0, "value" => $lpassword.2, "title" => $lpassword.1, "description" => $lpassword.3]
]}}
{{if $openid}}
  {{$form.elements[] = ["type" => "text", "name" => $lopenid.0, "value" => $lopenid.2, "title" => $lopenid.1, "description" => $lopenid.3]}}
{{/if}}
{{$form.elements[] = ["type" => "checkbox", "name" => $lremember.0, "value" => $lremember.2, "title" => $lremember.1, "description" => $lremember.3]}}
{{foreach $hiddens as $k=>$v}}
  {{$form.elements[] = ["type" => "hidden", "name" => $k, "value" => $v]}}
{{/foreach}}
{{$form.submitValue = $login}}
{{$formPart = ["type" => "form", "content" => $form, "layer" => 1]}}

{{$widget = ["parts" => [$formPart], "layer" => 1, "isSideWidget" => false]}}
{{$widget.parts[] = ["type" => "button", "content" => ["action" => "register", "title" => $register.desc]]}}
{{$widget.parts[] = ["type" => "button", "content" => ["action" => "lostpass", "title" => $lostlink]]}}

{{include file="FTI_Widget.tpl" widget=$widget}}

<!--
<form action="{{$dest_url}}" method="post" style="display:block;">
  <input type="hidden" name="auth-params" value="login" />
    
    
  {{include file="field_input.tpl" field=$lname}}
  {{include file="field_password.tpl" field=$lpassword}}
  
  {{if $openid}}
  {{include file="field_openid.tpl" field=$lopenid}}
  {{/if}}
  
  {{include file="field_checkbox.tpl" field=$lremember}}
  
  {{foreach $hiddens as $k=>$v}}
  <input type="hidden" name="{{$k}}" value="{{$v|escape:'html'}}" />
  {{/foreach}}

  <div style="text-align:center;">
    <input class="Field RoundCorners" style="width:20em;" type="submit" name="submit" value="{{$login|escape:'html'}}" />
  </div>

  <div style="margin-top:1em;text-align:center;">
    {{if $register}}
    <a style="width:15em;" class="Button RoundCorners" href="register" title="{{$register.title|escape:'html'}}">{{$register.desc}}</a>
    {{/if}}
    <a style="width:15em;" class="Button RoundCorners" href="lostpass" title="{{$lostpass|escape:'html'}}">{{$lostlink}}</a>
  </div>
</form>
-->