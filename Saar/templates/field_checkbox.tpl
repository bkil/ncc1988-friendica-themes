<div class="FieldRow">
  <div class="FieldCell">{{$field.1}}</div>
  <div class="FieldCell">
    <input 
      type="checkbox" name="{{$field.0}}" value="1"
      {{if $field.2 == 1 }}checked="checked"{{/if}}
      {{if $field.4 eq 'required'}} required="required"{{/if}}
      {{if $field.5 eq 'autofocus'}} autofocus="autofocus"{{/if}}
    />
    <label for="{{$field.0}}" class="Border RoundCorners"></label>
    <div class="FieldHint">{{$field.3}}</div>
  </div>
</div>
