{{$widget = ["title" => $profile.name, "isSideWidget" => true]}}
{{if $pdesc}}
{{$widget.description = $profile.pdesc}}
{{/if}}

{{$widget.parts = []}}
{{$photo = ["url" => $profile.url, "description" => $profile.name ]}}
{{if $profile.picdate}}
{{$photo.source = "`$profile.photo`?rev=`$profile.picdate`"}}
{{else}}
{{$photo.source = $profile.photo}}
{{/if}}
{{$widget.parts[] = ["type" => "image", "content" => $photo]}}

{{if $profile.network_name}}
{{$widget.parts[] = ["type" => "text", "content" => ["text" => $profile.network_name]]}}
{{/if}}

{{if $profile.edit}}
{{$widget.parts[] = ["type" => "button", "content" => ["action" => $profile.edit.0, "title" => $profile.edit.1]]}}
{{/if}}
{{include file="FTI_Widget.tpl" widget=$widget}}


{{$widget2 = ["parts" => [], "isSideWidget" => true]}}
{{if $homepage}}
{{$widget2.parts[] = ["type" => "link", "content" => ["action" => $profile.homepage, "title" => $profile.homepage]]}}
{{/if}}

{{$gmText = ""}}
{{if $gender}}
  {{$gmText = $profile.gender}}
{{/if}}
{{if $marital}}
  {{$gmText = "`$gmText`, `$profile.marital`"}}
{{/if}}
{{$widget2.parts[] = ["type" => "text", "content" => ["text" => $gmText]] }}
{{include file="FTI_Widget.tpl" widget=$widget2}}

{{if $location}}
{{$widgetL = ["parts" => 
  [["type" => "html",
   "content" => "<ul class=\"WidgetList\"><li>`$profile.address`</li><li>`$profile.locality`</li><li>`$profile.region`</li><li>`$profile.country_name`</li></ul>"
   ]],
   "isSideWidget" => true
  ]
}}
{{include file="FTI_Widget.tpl" widget=$widgetL}}
{{/if}}

{{if $about}}
{{$widgetA = ["parts" => [["type" => "text", "content" => ["text" => $profile.about]]], "isSideWidget" => true]}}
{{include file="FTI_Widget.tpl" widget=$widgetA}}
{{/if}}

{{if $connect}}
{{$widgetConn = ["parts" => [["type" => "button", "content" => 
  [
    "action" => "dfrn_request/`$profile.nickname`",
    "title" => $connect
  ]
]], "isSideWidget" => true]
}}
{{include file="FTI_Widget.tpl" widget=$widgetConn}}
{{/if}}

{{if $profile.contact}}
{{$widgetContact = ["parts" => [["type" => "text", "content" => ["text" => $profile.contact]]], "isSideWidget" => true]}}
{{include file="FTI_Widget.tpl" widget=$widgetContact}}
{{/if}}

{{$contact_block}}

