<div class="Popup Background JS" id="Popup_Follow">
  <div class="PopupActions">
    <img style="width:2em;height:2em;" src="images/icons/delete.png" onclick="javascript:void(FriendicaUI.Popup.Close('Follow'));" alt="X"></img>
  </div>
  <h3 class="PopupHeadline">{{$connect}}</h3>
  <div class="PopupDescription">{{$desc}}</div>
  <form class="PopupContent" action="follow" method="get">
    <input class="Field RoundCorners" type="text" name="url" value="{{$value|escape:'html'}}" placeholder="{{$hint|escape:'html'}}" title="{{$hint|escape:'html'}}" />
    <input class="Button RoundCorners BigText" type="submit" name="submit" value="{{$follow|escape:'html'}}" />
  </form>
</div>
<div class="Button RoundCorners JS" onclick="javascript:void(FriendicaUI.Popup.Open('Follow'));">{{$connect}}</div>
<noscript>
  {{$widget = [ "title" => $connect, "description" => $desc, "isSideWidget" => true,
    "parts" =>  [["type" => "form", 
                  "content" =>  ["method" => "get", "action" => "follow",
                  "elements" => [["type" => "text", "name" => "url", "value" => $value, "placeholder" => $hint]],
                  "submitValue" => $follow
                  ]
               ]]
             ]
  }}
  {{include file="FTI_Widget.tpl" widget=$widget}}
</noscript>
