<a name="{{$item.id}}"></a>
  <div class="PostContent" id="Post-{{$item.id}}">
    <div class="PostHeader BottomLine">
      <span class="PostOwnerInfo MenuActivator">
        <a class="Link" href="{{$item.profile_url}}" target="redir" title="{{$item.linktitle}}">
          <img class="SmallIcon RoundCorners" src="{{$item.thumb}}" alt="{{$item.name}}" />
          <span>{{$item.name}}</span>
        </a>
        {{if not $item.owner_url}}
        <ul id="PostOwnerMenu-{{$item.id}}" class="Border Menu">
          {{$item.item_photo_menu}}
        </ul>
        {{/if}}
      </span>
      <span class="PostDetails">
        <span class="PostTimePassed" title="{{$item.localtime}}">{{$item.ago}}</span>
      </span>
    </div>
    <div class="PostContent">
      <h3 class="Headline Underline">{{$item.title}}</h3>
      <div class="PostBody">
        {{$item.body}}
      </div>
      {{if $item.has_cats}}
        <div class="PostTags">
          {{foreach $item.categories as $c}}
            {{$c.name}}
            {{if $c.removeurl}}
              <a href="{{$c.removeurl}}" title="{{$remove}}">[{{$remove}}]</a>
            {{/if}}
            &nbsp;
          {{/foreach}}
        </div>
      {{/if}}
      {{if $item.has_folders}}
        <div class="PostFiles">
          {{if $item.drop.dropping}}
          <a class="Icon ActionPostDrop" href="item/drop/{{$item.id}}" onclick="return confirmDelete();"
            title="{{$item.drop.delete}}" onmouseover="imgbright(this);" onmouseout="imgdull(this);">
          </a>
          {{/if}}
          {{if $item.drop.pagedrop}}
          <input class="PostFileSelect" type="checkbox" onclick="checkboxhighlight(this);" title="{{$item.drop.select}}"
            name="itemselected[]" value="{{$item.id}}" />
          {{/if}}
          {{if $item.conv}}
            <a href="{{$item.conv.href}}" title="{{$item.conv.title}}">{{$item.conv.title}}</a>
          {{/if}}
        </div>
      {{/if}}
      <div class="PostInfos">
        <span class="PostInfoField SmallText">{{$item.like}}</span>
        {{if $item.cool}}
        <span class="PostInfoField SmallText">{{$item.cool}}</span>
        {{/if}}
        <span class="PostInfoFiled SmallText">{{$item.dislike}}</span>
      </div>
      <div class="PostActions SmallText">
        <img class="Button RoundCorners PostAction" title="{{$item.vote.like.1|escape:'html'}}"
          onclick="javascript:void(FriendicaUI.Post.Mark({{$item.id}},'like'));"
          src="./view/theme/Saar/icons/Like.svg"></img>
        {{if $item.vote.cool}}
        <span class="Button RoundCorners PostAction" title="{{$item.vote.cool.0|escape:'html'}}"
          onclick="javascript:void(FriendicaUI.Post.Mark({{$item.id}},'cool'));">Cool</span>
        {{/if}}
        {{if $item.vote.dislike}}
        <img class="Button RoundCorners PostAction" title="{{$item.vote.dislike.0|escape:'html'}}"
          onclick="javascript:void(FriendicaUI.Post.Mark({{$item.id}},'dislike'));"
          src="./view/theme/Saar/icons/Dislike.svg"></img>
        {{/if}}
        {{if $item.vote.share}}
        <img class="Button RoundCorners PostAction" title="{{$item.vote.share.0|escape:'html'}}"
          onclick="javascript:void(FriendicaUI.Post.Reshare({{$item.id}}));"
          src="./view/theme/Saar/icons/Reshare.svg"></img>
        {{/if}}
      </div>
    </div>
    {{foreach $item.children as $child}}
      {{include file="{{$child.template}}" item=$child}}
    {{/foreach}}
    
    {{if $item.flatten}}
      {{if $item.comment}}
      <div class="ActionComment Border Layer3 RoundCorners">
        {{$item.comment}}
      </div>
      {{/if}}
    {{/if}}
  </div>