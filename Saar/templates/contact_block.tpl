{{$widget = ["title" => $contacts, "parts" => []]}}
{{if $micropro}}
{{$pic = ""}}
  {{foreach $micropro as $m}}
    <!-- can't be reformatted (unknown/missing template name) -->
    {{$pic = "`$pic``$m`"}}
  {{/foreach}}
{{$widget.parts[] = ["type" => "html", "content" => $pic]}}
{{else}}
{{$widget.parts[] = ["type" => "button", "content" => ["action" => "viewcontacts/`$nickname`", "title" => $viewcontacts]]}}
{{/if}}
{{include file="FTI_Widget.tpl" widget=$widget}}
