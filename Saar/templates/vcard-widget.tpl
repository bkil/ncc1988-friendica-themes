{{$widget = ["title" => {{$name}}, "parts" => [["type" => "image", "content" => null]] ]}}
{{$img = ["source" => $photo, "description" => $name]}}
{{if $url}}
{{$img.url = $url}}
{{/if}}
{{$widget.parts[0].content = $img}}
{{include file="Widget.tpl" widget=$widget}}
