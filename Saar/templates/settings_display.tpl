{{$form = ["action" => "settings/display", "method" => "post", "submitValue" => $submit]}}
{{$imageSelector = ["type" => "themeSelect", "name" => $theme.0, "description" => $theme.3, "jsEvents" => [["event" => "onchange", "action" => "javascript:void(FriendicaUI.Settings.PreviewTheme(this, 'Settings_Display_Preview'));"]], "option" => [], "hasPreview" => true]}}
{{foreach $theme.4 as $opt=>$val}}
  {{$option = ["label" => $opt, "value" => $val]}}
  {{if $opt==$theme.2}}
    {{$option.selected = true}}
  {{/if}}
  {{$imageSelector.option[] = $option}}
{{/foreach}}

{{$mobileThemeSelect = ["type" => "select", "name" => $mobile_theme.0, "description" => $mobile_theme.3, "title" => $mobile_theme.1, "value" => $mobile_theme.2, option => []]}}
{{foreach $mobile_theme.4 as $opt=>$val}}
  {{$option =  ["label" => $opt, "value" => $val]}}
  {{if $opt==$mobile_theme.2}}
    {{$option.selected = true}}
  {{/if}}
  {{$mobileThemeSelect.option[] = $option}}
{{/foreach}}


{{$form.elements = [
  $imageSelector,
  ["type" => "number", "name" => $itemspage_network.0, "description" => $itemspage_network.3, "title" => $itemspage_network.1,
    "value" => $itemspage_network.2],
  $mobileThemeSelect,
  ["type" => "number", "name" => $itemspage_mobile_network.0, "description" => $itemspage_mobile_network.3, 
    "title" =>  $itemspage_mobile_network.1, "value" => $itemspage_mobile_network.2],
  ["type" => "number", "name" => $ajaxint.0, "description" => $ajaxinit.3, "title" => $ajaxinit.1, "value" => $ajaxinit.2],
  ["type" => "checkbox", "name" => $no_auto_update.0, "description" => $no_auto_update.3, "title" => $no_auto_update.1,
    "value" => $no_auto_update.2],
  ["type" => "checkbox", "name" => $nosmile.0, "description" => $nosmile.3, "title" => $nosmile.1,
    "value" => $nosmile.2],
  ["type" => "checkbox", "name" => $noinfo.0, "description" => $noinfo.3, "title" => $noinfo.1,
    "value" => $noinfo.2],
  ["type" => "checkbox", "name" => $infinite_scroll.0, "description" => $infinite_scroll.3, "title" => $infinite_scroll.1,
    "value" => $infinite_scroll.2],
  ["type" => "hidden", "name" => "form_security_token", "value" => $form_security_token]
]}}
{{$formPart = ["type" => "form", "content" => $form]}}
{{$widget = ["title" => $ptitle, "parts" => [$formPart]]}}
{{include file="FTI_Widget.tpl" widget=$widget}}
<!--
<h1>{{$ptitle}}</h1>
<form action="settings/display" method="post">
  <input type='hidden' name='form_security_token' value='{{$form_security_token}}'>

  <div class="Border RoundCorners Padding">
    {{include file="field_themeselect.tpl" field=$theme}}
    {{include file="field_input.tpl" field=$itemspage_network}}
    {{include file="field_themeselect.tpl" field=$mobile_theme}}
    {{include file="field_input.tpl" field=$itemspage_mobile_network}}
    {{include file="field_input.tpl" field=$ajaxint}}
    {{include file="field_checkbox.tpl" field=$no_auto_update}}
    {{include file="field_checkbox.tpl" field=$nosmile}}
    {{include file="field_checkbox.tpl" field=$noinfo}}
    {{include file="field_checkbox.tpl" field=$infinite_scroll}}
    <div class="Center">
      <input type="submit" name="submit" class="Button RoundCorners" value="{{$submit|escape:'html'}}" />
    </div>
  </div>

  {{if $theme_config}}
  <h2>Theme settings</h2>
  <div class="Border RoundCorners Padding">
    {{$theme_config}}
  </div>
  {{/if}}
</form>
-->