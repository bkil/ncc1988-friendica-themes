This is my collection of themes for the social network friendica.

Every directory in this repository represents a theme.

Check the LICENSE file in each theme for its license.
Probably most of them are licensed as CC0/Public Domain.

