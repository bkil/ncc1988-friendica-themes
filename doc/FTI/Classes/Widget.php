<?php

include ('./Content.php');


class Widget
{
  public $idAttr = null; //if set to something not null then the HTML id attribute will be set to this
  public $isSideWidget = false; //if the widget is displayed on the side (true) or on the main content (false)
  public $layer = 1; //a layer: from 1 to 3 whereby 1 is the top layer, 3 the bottom layer
  
  //the following things will be rendered in the widget template:
  public $title = null; //a title string (optional)
  public $description = null; //a description text (optional)
  public $content = []; //Array of Content instances: A widget can have more than one content instance
}


?>