<?php

class Content
{
  public $type = null;
  public $jsEvents = []; //Array of JsEvent instances
}

/**
  A class for links.
  The attribute isJsLink defines if the link is a JavaScript link (a link to a JavaScript function).
  With this attribute the link can be drawn differently by a template.
**/
class Content_Link extends Content
{
  public $type = "Link";
  public $action = "";
  public $title = "";
  public $isJsLink = false;
  public $iconUrl = null; //the link may be represented through an icon instead of text
  
};

/// Since buttons can be highlighted and Links cannot there is a special class for buttons
class Content_Button extends Content_Link
{
  public $type = "Button";
  public $selected = false; //if set to true the button can be highlighted in the theme
}

class Content_ButtonGroup extends Content
{
  public $type = "ButtonGroup";
  public $buttons = []; //array of Content_Button instances
}


class Content_Image extends Content
{
  public $type = "Image";
  public $actionUrl = ""; //if the user should be redirected to another URL if he clicks the image then the URL can be specified here
  public $source = ""; //the image URL
  public $description = ""; //image description (used in the HTML alt attribute)
  
}

class Content_Text extends Content
{
  public $type = "Text";
  public $size = ""; //may be set to 2 strings: 'small' => smaller-than-normal text size, 'big': bigger-than-normal text size
  public $text = ""; //the text that shall be displayed
}

class Content_FormattedText extends Content_Text
{
  /* the difference between Text and FormattedText is that FormattedText contains pre-formatted HTML */
  public $type = "FormattedText";
}

/// for HTML forms and their content
class Content_Form extends Content
{
  public $type = "Form";
  public $elements = [];  //array of FormElement instances
  public $action = "";    //content of the HTML action attribute
  public $method = "";    //content of the HTML method attribute
  public $name = "";      //content of the HTML name attribute
  public $submitValue = ""; //the text of the submit button (the value of the submit input element)
  public $specialSubmitName = null; //if the form's submit button has a special name it can be set here
}


class JsEvent
{
  public $event = ""; //the event name (e.g. onclick, onmouseover, onkeypress, ...
  public $action = ""; //the javascript code or function that shall be executed on the event
}

?>