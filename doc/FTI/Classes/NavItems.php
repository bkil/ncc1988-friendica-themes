<?php

include ('./Content.php');

/// base class for all navigation items
class NavItem
{
  public $type = null; //string
  public $templateName = 'FTI_NavItem';
  public $content = null;
}


/// a class for menus in the navigation bar
class NavItem_Menu extends NavItem
{
  public $type = 'menu';
  public $content = []; //array of Content_Link
}

/// links in the navigation bar
class NavItem_Link extends NavItem
{
  public $type = 'link';
  public $content = null; //a Content_Link instance belongs here
}

/// a class for simple text in the nav bar
class NavItem_Text extends NavItem
{
  public $type = 'text';
  public $content = null; // a Content_Text instance belongs here
}

?>